const DB = require('../db')
const user = require('../models/user')
const validations = require('../utils/validations')

const login = async(req, res) => {
    try {
        let response = validations.loginValidation(req.body.username, req.body.password)
        if (response.success) {
            response = await user.login(req.body.username, req.body.password)
        }
        res.json(response)
    } catch (err) {
        console.log(err);
        res.json({ success: false, msg: err })
    }
}

const register = async(req, res) => {
    try {
        let response = validations.registerValidation(req.body.username, req.body.password, req.body.email)
        if (response.success) {
            response = await user.register(req.body.username, req.body.password, req.body.email)
        }
        res.json(response)
    } catch (err) {
        console.log(err);
        res.json({ success: false, msg: err })
    }
}

const logout = async(req, res) => {
    if (!req.body.refresh_token) {
        res.json({ success: false, msg: "invalid token" })
        return
    }
    user.logout(!req.body.refresh_token)
    res.json({ success: true, msg: "success logout" })
}

module.exports = {
    login,
    register,
    logout
}