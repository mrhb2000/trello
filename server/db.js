"use strict";
let db;
const { MongoClient } = require("mongodb");
const uri = "mongodb://127.0.0.1:27017";
const client = new MongoClient(uri, { useUnifiedTopology: true });

async function initDB() {
    try {
        await client.connect();
        const DB = client.db("trello");
        return DB;
    } catch (error) {
        console.log(error);
    }
}

async function getInstance() {
    if (!db) {
        db = await initDB();
    }
    return db;
}

async function insertOne(collection, dataToInsert) {
    const db = await getInstance();
    const res = await db.collection(collection).insertOne(dataToInsert);
    return res;
}

async function updateOne(collection, dataToUpdate, condition) {
    const db = await getInstance();
    const res = await db
        .collection(collection)
        .updateOne(condition, { $set: dataToUpdate }, { upsert: true });
    return res;
}

async function findOne(collection, condition) {
    const db = await getInstance();
    const res = await db.collection(collection).findOne(condition);
    return res;
}

async function deleteOne(collection, condition) {
    const db = await getInstance();
    const res = await db.collection(collection).deleteOne(condition);
    return res;
}

async function getAll(collection, condition) {
    const db = await getInstance()
    const res = await db.collection(collection).find(condition)
    return res
}
const DB = {
    'insertOne': insertOne,
    'updateOne': updateOne,
    'findOne': findOne,
    'deleteOne': deleteOne,
    'getAll': getAll
}

module.exports = DB