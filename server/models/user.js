 const DB = require('../db')
 const tokenManager = require('../security/tokenManager')

 const login = async(_username, _password) => {
     try {
         const user = await DB.findOne("users", { username: _username })
         if (!user || !user.username) {
             return { success: false, msg: "user not found" }
         }
         if (user.password !== _password) {
             return { success: false, msg: "invalid password" }
         }
         const result = await tokenManager.generateToken(user._id)
         return result
     } catch (err) {
         console.log(err);
         return { success: false, msg: err }
     }
 }

 const register = async(_username, _password, _email) => {
     try {

         if (await isExsitUsername(_username)) {
             return { success: false, msg: "username already exists" }
         }
         if (await isExsitEmail(_email)) {
             return { success: false, msg: "this email already exists" }
         }
         await DB.insertOne("users", { username: _username, password: _password, email: _email })
         const response = await login(_username, _password)
         return response
     } catch (err) {
         console.log(err);
         return { success: false, msg: err }
     }
 }

 async function isExsitUsername(_username) {
     const user = await DB.findOne("users", { username: _username })
     if (!user) {
         return false
     }
     return true
 }

 async function isExsitEmail(_email) {
     const user = await DB.findOne("users", { email: _email })
     if (!user) {
         return false
     }
     return true
 }

 const logout = async(refresh_Token) => {
     DB.deleteOne('tokens', { refresh_token: refresh_Token })
 }

 module.exports = {
     login,
     register,
     logout
 }