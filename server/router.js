const router = require("express").Router()
const user = require("./controller/user")
const tokenManager = require("./security/tokenManager")
const checkBody = require('./utils/validations').checkBody

router.use(checkBody)
router.post("/login", user.login)
router.post("/register", user.register)
router.get("/refresh-token", tokenManager.refreshToken)

module.exports = router