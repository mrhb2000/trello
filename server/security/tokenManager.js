'use strict';
const DB = require('../db')
const jwt = require('jsonwebtoken');

const generateToken = async(_userID) => {
    const access_Token = await accessTokenGenerator(_userID)
    const refresh_Token = jwt.sign({ id: _userID }, 'mrhb2000_r', { expiresIn: "10d" });
    try {
        const result = await DB.updateOne("tokens", { refreshToken: refresh_Token }, { userID: _userID })
        if (!result) {
            return { success: false, msg: "database error" }
        }
    } catch (err) {
        console.log(err);
        return { success: false, msg: err }
    }
    return { success: true, access_token: access_Token, refresh_token: refresh_Token };
}

const tokenValidation = (req, res, next) => {
    try {
        const token = req.headers.access_token;
        if (!token) {
            res.status(401).json({ success: false, msg: "invalid token" })
            return
        }
        jwt.verify(token, "mrhb2000_a");
        next()
    } catch (err) {
        console.log(err);
        res.status(403).json({ success: false, msg: "invalid token" })
        return
    }
}

const accessTokenGenerator = async(userID) => {
    const access_Token = await jwt.sign({ id: userID }, 'mrhb2000_a', { expiresIn: "15min" });
    return access_Token
}


const refreshToken = async(req, res) => {
    try {
        const rToken = req.headers.refresh_token;
        if (!rToken) {
            res.status(401).json({ success: false, error: "token not found" })
            return
        }
        const rDecoded = jwt.verify(rToken, "mrhb2000_r");
        if (!rDecoded) {
            res.status(401).json({ success: false, error: "invalid refresh token" })
            return
        }
        const access_Token = await accessTokenGenerator(rDecoded.id)
        res.json({ success: true, msg: "new token created", access_token: access_Token })
    } catch (err) {
        console.log(err);
        res.status(500).json({ success: false, msg: err })
    }
}

module.exports = { generateToken, tokenValidation, refreshToken }