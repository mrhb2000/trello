const checkBody = (req, res, next) => {
    if (!req.body || Object.keys(req.body).length == 0) {
        res.json({ succses: false, msg: "empty body" })
        return
    }
    next()
}

const loginValidation = (username, password) => {
    if (!username || !password) {
        return { succses: false, msg: "request parameters are invalid" }
    }

    if (username.length < 3) {
        return { succses: false, msg: "invalid username" }
    }
    if (password.length < 8) {
        return { success: false, msg: "invalid password" }
    }
    return { success: true }
}

const registerValidation = (username, password, email) => {
    if (!username || !password || !email) {
        return { success: false, msg: "invalid params in json" }
    }
    if (username.length < 3) {
        return { success: false, msg: "username " + username + " must be longer than 3 characters" }
    }
    if (password.length < 8) {
        return { success: false, msg: "password must be longer than 8 characters" }
    }
    if (!validateEmail(email)) {
        return { success: false, msg: "invalid email" }
    }
    return { success: true }

}



function validateEmail(email) {
    const re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}


module.exports = {
    checkBody,
    loginValidation,
    registerValidation
}